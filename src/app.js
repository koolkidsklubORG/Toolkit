import {
  app,
  BrowserWindow,
  ipcMain,
  Tray,
  Menu
} from 'electron';
const moment = require('moment'),
  status = {
    "update": false,
    "main": false
  },
  waitUntil = require('wait-until');

if (require('electron-squirrel-startup')) {
  app.quit();
}

let mainWindow, loadingWindow, appIcon;

const createWindow = () => {
  mainWindow = new BrowserWindow({
    width: 1600,
    height: 1000,
    backgroundColor: "#2c2c2c",
    show: false,
    webPreferences: {
      nativeWindowOpen: true
    }
  });

  loadingWindow = new BrowserWindow({
    width: 250,
    height: 300,
    backgroundColor: "#2c2c2c",
    show: false,
    frame: false
  });


  appIcon = new Tray(__dirname + '/app/assets/img/app.ico');

  var contextMenu = Menu.buildFromTemplate([
    {
      label: 'Top Secret Control Panel',
      click: function() { },
      enabled: false
    },
    {
      label: 'Toggle DevTools',
      accelerator: 'Alt+Command+I',
      click: function () {
        mainWindow.show();
        mainWindow.toggleDevTools();
      }
    },
    {
      label: 'Quit',
      accelerator: 'Command+Q',
      selector: 'terminate:',
      click: function() {
        app.quit();
      }
    }
  ]);
  appIcon.setToolTip('PhotonDrop');
  appIcon.setContextMenu(contextMenu);
  appIcon.on('double-click', () => {
    if(mainWindow.isVisible()) {
      mainWindow.hide();
    } else {
      mainWindow.show();
    }
  });


  loadingWindow.on('closed', () => {
    loadingWindow = null;
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  mainWindow.webContents.on('new-window', (event, url) => {
    event.preventDefault();
    const win = new BrowserWindow({
      show: false
    });
    win.loadURL(url);
    win.show();
    event.newGuest = win;
  });

  mainWindow.loadURL(`file://${__dirname}/app/index.html`);
  loadingWindow.loadURL(`file://${__dirname}/app/loading.html`);
  loadingWindow.show();
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on('photondrop-main-ready', (e, data) => {
  status.main = true;
});

ipcMain.on('photondrop-update-ready', (e, data) => {
  status.update = true;
});

ipcMain.on('response-session-info', (e, data) => {
    updatePresence(data);
});

waitUntil()
  .times(20)
  .interval(500)
  .condition(function (cb) {
    process.nextTick(function () {
      cb((status.main === true && status.update === true)?true:false);
    });
  })
  .done(function (result) {
    if (mainWindow !== null && loadingWindow !== null) {
      loadingWindow.hide();
      mainWindow.show();
      loadingWindow.close();
    }
  });

const DiscordRPC = require('discord-rpc');
const ClientId = '401251903562055691';

DiscordRPC.register(ClientId);

const rpc = new DiscordRPC.Client({
  transport: 'ipc'
});
const startTimestamp = new Date();
let appSettings = {};

async function updatePresence(session) {
  appSettings = await mainWindow.webContents.executeJavaScript('localStorage.getItem(\'settings\')').then(function(value){
    return JSON.parse(value);
  });
  if (session) {
    if (typeof session.created_time === "number" && typeof session.started_time === "object") {
      rpc.setActivity({
        details: session.location,
        state: '⏸ Waiting for dropees...',
        startTimestamp: new Date(session.created_time),
        smallImageKey: 'small_lobby_users',
        smallImageText: `${session.dropee_count} Dropees`,
        largeImageKey: `large_lobby_${session.type}`,
        largeImageText: `Hosting a${(/^(o|i)/g.test(session.type) ? 'n' : '')} ${session.type} lobby`,
        instance: false,
      });
    } else if (typeof session.created_time === "number" && typeof session.started_time === "number") {
      let startedTime = moment(session.started_time).toDate(),
        remaining = moment(startedTime).add(session.duration, 'm').toDate();
      rpc.setActivity({
        details: session.location,
        state: '▶ Currently in a drop',
        startTimestamp: startedTime,
        endTimestamp: remaining,
        smallImageKey: 'small_lobby_users',
        smallImageText: `with ${session.dropee_count} Dropees`,
        largeImageKey: `large_lobby_${session.type}`,
        largeImageText: `Hosting a${(/^(o|i)/g.test(session.type) ? 'n' : '')} ${session.type} lobby`,
        instance: false,
      });
    }
  } else {
    if((appSettings.discord.preparing == 'true')) {
      rpc.setActivity({
        details: `◌ Preparing a drop...`,
        startTimestamp,
        largeImageKey: 'large_lobby_preparing',
        largeImageText: 'Don\'t ever buy SharkCards.',
        instance: false,
      });
    } else {
      rpc.setActivity({});
    }
  }
}

rpc.on('ready', () => {
  setInterval(() => {
    mainWindow.webContents.send('request-session-info');
  }, 15e3);
  mainWindow.webContents.send('request-session-info');
});

rpc.login(ClientId).catch(console.error);