window.$ = window.jQuery = require('jquery');
window.Dropzone = require('dropzone');
window.moment = require('moment');
window.countdown = require('countdown');
require( 'datatables.net-bs' )();
window.shell = require('electron').shell;

$(function () {
  var PhotonDrop = window.PhotonDrop = PhotonDrop || require(__dirname + '/assets/js/PhotonDrop');
  PhotonDrop.Site.initialize();
});