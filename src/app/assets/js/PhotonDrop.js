module.exports = {
    Site: require(__dirname + '/modules/Site'),
    Storage: require(__dirname + '/modules/Storage'),
    Common: require(__dirname + '/modules/Common'),
    UserManager: require(__dirname + '/modules/UserManager'),
    Session: require(__dirname + '/modules/Session'),
    IPC: require(__dirname + '/modules/IPC'),
    Texts: require(__dirname + '/modules/Texts'),
};