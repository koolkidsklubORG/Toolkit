module.exports = {
    getTextById: (ID) => {
        return PhotonDrop.Storage.queryAll("predefined_texts", { query:{ID}, limit:1 })[0];
    },
    
    addText: (title, content, scope) => {
        PhotonDrop.Storage.insert("predefined_texts", {
            title,
            content,
            scope
        });
        return PhotonDrop.Storage.commit();
    },
    
    editTextById: (ID, title, content, scope) => {
        PhotonDrop.Storage.update("predefined_texts", {ID}, function(row) {
            row.title = title;
            row.content = content;
            row.scope = scope;
            return row;
        });
        return PhotonDrop.Storage.commit();    
    },

    deleteTextById: (ID) => {
        PhotonDrop.Storage.deleteRows("predefined_texts", {ID});
        return PhotonDrop.Storage.commit();
    },
    
    getTextsByScope: (scope) => {
        return PhotonDrop.Storage.queryAll("predefined_texts", { query:{scope} });
    }
}