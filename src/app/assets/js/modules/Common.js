module.exports = {
    dropBotRegEx: /Discord: (.*) - ID: (\d*) - SC: (.*) - Drops Attended: (\d*)/g,
    UUID: require('uuid/v4')(),
    shortUUID: require('uuid/v4')().split("-")[0],
    Clipboard: require('electron').clipboard
};