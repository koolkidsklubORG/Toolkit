function initialize() {
    $('body > .px-nav').pxNav({
        storeState: false
    });
    $('body > .px-footer').pxFooter();

    if(!localStorage.getItem('soundscheme')) {
        localStorage.setItem('soundscheme', 'default');
    }
    if(!localStorage.getItem('theme')) {
        localStorage.setItem('theme', 'default');
    }

    PhotonDrop.Site.applyTheme(localStorage.getItem('theme') || 'default');
    $("div.px-content").load('start.html');

    $('a[href], button[href]').click(function (event) {
        var url = $(this).attr('href');
        if (url.indexOf('#') == 0 || url.indexOf('javascript:') == 0) return;
        event.preventDefault();

        $("ul.px-nav-content > li").each(function () {
            $(this).removeClass("active");
        });

        $('div.px-content').fadeOut('fast', function () {
            $("div.px-content").load(url, () => {
                $('div.px-content').fadeIn('fast');
            });
        });

        $(this).closest(".px-nav-item").addClass("active");
    });

    setInterval(PhotonDrop.Site.updateClock, 500);
    $("html").fadeIn("slow");
    PhotonDrop.IPC.windowReady();
}

function applyTheme(theme) {
    let shade = (theme.includes('dark')) ? '-dark' : '';
    $("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "assets/css/bootstrap" + shade + ".min.css"
    }).appendTo("head");
    $("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "assets/css/pixeladmin" + shade + ".min.css"
    }).appendTo("head");
    $("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "assets/css/widgets" + shade + ".min.css"
    }).appendTo("head");
    $("<link/>", {
        rel: "stylesheet",
        type: "text/css",
        href: "assets/css/themes/" + theme + ".min.css"
    }).appendTo("head");
}

function updateClock() {
    $("ul.navbar-nav").find("a.ion-clock").html('&nbsp;' + moment().format("HH:mm:ss"));
}

const Alert = {
    toast: function (type, title, msg, sound = true) {
        toastr[type](msg, title, {
            positionClass: 'toast-bottom-right',
            closeButton: true,
            progressBar: true,
            preventDuplicates: true,
            newestOnTop: true,
        });
        $("ul.nav").find("div#navbar-notifications").prepend(`<div class="widget-notifications-item">
            <div class="widget-notifications-title text-info">${title}</div>
            <div class="widget-notifications-description">
                ${msg}
            <div class="widget-notifications-date">${moment().format("HH:mm:ss")}</div>
        </div>`);
        if (sound) PhotonDrop.Site.Alert.sound("alerts_notification");
    },
    sound: function (file, loop = false) {
        var audio = $("audio#alert-player");
        audio.attr("loop", loop);

        $("ul.nav").find("i.notifyBell").removeClass("ion-android-notifications-none text-muted").addClass("ion-android-notifications");
        audio.attr("src", `assets/sounds/${localStorage.getItem('soundscheme') || "default"}/${file}.wav`);
        audio[0].load();
        audio[0].oncanplaythrough = audio[0].play();
    },
    dismiss: function () {
        var audio = $("audio#alert-player");
        audio.attr("src", "");
        audio.attr("loop", false);
        toastr.clear();
        $("ul.nav").find("i.notifyBell").removeClass("ion-android-notifications").addClass("ion-android-notifications-none text-muted");
        PhotonDrop.Site.Alert.toast('info', 'Notifications silented', 'All notifications were dismissed', true);
        $("ul.nav").find("div#navbar-notifications").empty();
    },
    toggleMute: function () {
        var audio = $("audio#alert-player");
        audio[0].muted = !audio[0].muted;
        if (audio[0].muted) {
            $("ul.nav").find("i.muteToggle").removeClass("ion-android-volume-up").addClass("ion-android-volume-off");
        } else {
            $("ul.nav").find("i.muteToggle").removeClass("ion-android-volume-off").addClass("ion-android-volume-up");
        }
    }
}

module.exports = {
    initialize,
    applyTheme,
    updateClock,
    smalltalk: require('smalltalk'),
    Alert,
    Settings: () => { return JSON.parse(localStorage.getItem('settings')) }
}