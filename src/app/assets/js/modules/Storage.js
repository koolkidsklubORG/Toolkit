const localStorageDB = require('localstoragedb');
const database = new localStorageDB("photondrop", localStorage);

if (database.isNew()) {
    database.createTable("user", ["userid", "username", "socialclub", "attended", "blacklist", "firstseen", "lastseen", "notes"]);
    database.createTable("user_warns", ["sessionid", "userid", "reason", "time"]);
    database.createTable("session", ["type", "duration", "location", "status", "created_time", "started_time", "ended_time"]);
    database.createTable("session_dropees", ["sessionid", "userid"]);
    database.createTable("predefined_texts", ["title", "content", "scope"]);
    database.commit();
}

if (!localStorage.getItem('settings')) {
    localStorage.setItem('settings', JSON.stringify({
        threshold: {
            warns: 3,
            money: 70000000
        },
        discord: {
            preparing: true
        },
        announcements_with_mention: true
    }));
} else {
    let settings = JSON.parse(localStorage.getItem('settings'));
    if(!settings.discord.hasOwnProperty('enhanceddiscord')) settings.discord.enhanceddiscord = false;
    localStorage.setItem('settings', JSON.stringify(settings));
}

module.exports = database;