function add(userid, username, socialclub, attended) {
    if(!PhotonDrop.UserManager.exists(userid)) {
        PhotonDrop.Storage.insert("user", {
            userid,
            username,
            socialclub,
            attended: parseInt(attended),
            blacklist: false,
            firstseen: new Date().valueOf(),
            lastseen: new Date().valueOf(),
            notes: ""
        });
    } else if(PhotonDrop.UserManager.exists(userid)) {
        PhotonDrop.Storage.update("user", { userid }, function(row) {
            row.username = username;
            row.socialclub = socialclub;
            row.attended = parseInt(attended);
            row.lastseen = new Date().valueOf();
            return row;
        });
    }
    return PhotonDrop.Storage.commit();
}

function exists(userid) {
    return (PhotonDrop.Storage.queryAll("user", { query: { userid }, limit: 1 } ).length > 0)?true:false;
}

function edit(userid, args) {
    if (PhotonDrop.UserManager.exists(userid)) {
        PhotonDrop.Storage.update("user", {userid}, function(row) {
            Object.keys(args).forEach((key) => {
                row[key] = args[key];
            });
            return row;
        });
        return PhotonDrop.Storage.commit();
    }
    return false;
}

function get(userid) {
    if (PhotonDrop.UserManager.exists(userid)) {
        let user = PhotonDrop.Storage.queryAll("user",{
            query: { userid }, limit: 1
        })[0];
        user.attended_internal = PhotonDrop.Storage.queryAll("session_dropees", { query: { userid } });
        user.warns = PhotonDrop.Storage.queryAll("user_warns", { query: { userid } });

        return user;
    }
    return false;
}


function warn(sessionid, userid, reason) {
    if (PhotonDrop.UserManager.exists(userid)) {
        PhotonDrop.Storage.insert("user_warns", { sessionid, userid, reason, time: new Date().valueOf() });
        return PhotonDrop.Storage.commit();
    }
    return false;
}

function blacklist(userid) {
    if (PhotonDrop.UserManager.exists(userid)) {
       let user = PhotonDrop.UserManager.get(userid);
       PhotonDrop.Storage.update("user", {userid}, function(row) {
            row.blacklist = !user.blacklist;
            return row;
       });
       return PhotonDrop.Storage.commit();
    } else {
        throw new Error("User not found");
    }
}

function remove(userid) {
    if (PhotonDrop.UserManager.exists(userid)) {
        PhotonDrop.Storage.deleteRows("user", { userid: userid });
        return PhotonDrop.Storage.commit();
    } else {
        throw new Error("User not found");
    }   
}

module.exports = {
    add,
    exists,
    edit,
    get,
    warn,
    blacklist,
    remove,
};