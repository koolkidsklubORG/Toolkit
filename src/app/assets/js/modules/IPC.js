const { ipcRenderer } = require('electron');

ipcRenderer.on('request-session-info', function(event, args) {
    let sessionStatus = PhotonDrop.Session.status();
    if(sessionStatus) {
        let dropeeCount = PhotonDrop.Storage.queryAll("session_dropees", { query: { sessionid: sessionStatus.ID } });
        sessionStatus.dropee_count = (dropeeCount)?dropeeCount.length:0;
    }
    ipcRenderer.send('response-session-info', sessionStatus);
});

module.exports = {
    windowReady: function () {
        ipcRenderer.send('photondrop-window-ready', true);
    }
}