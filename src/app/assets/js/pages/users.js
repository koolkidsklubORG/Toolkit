var userList = $("#usersTable").DataTable();
var userInfo = null;

$(() => {
    populateUserList();
    userInfo = $("div.widget-profile").clone();

    $("div#modal-add-user").find("form").submit(function (e) {
        e.preventDefault();

        let userid = $(this).find("input#input-discord-id").val();
        let username = $(this).find("input#input-discord-username").val();
        let socialclub = $(this).find("input#input-socialclub").val();
        let attended = $(this).find("input#input-drops-attended").val();

        if (PhotonDrop.UserManager.add(userid, username, socialclub, attended)) {
            if ($(this).find("input#input-blacklist").prop('checked')) {
                PhotonDrop.UserManager.blacklist(userid);
            }
            $(this).closest('div#modal-add-user').modal('toggle');
            PhotonDrop.Site.smalltalk.alert('Success', 'User ' + username + ' saved to database');
            populateUserList();
        } else {
            PhotonDrop.Site.smalltalk.alert('Error', 'User wasn\'t saved to database. Maybe already exists?');
        }
    });
});

function populateUserList() {
    userList.clear();
    PhotonDrop.Storage.queryAll("user").forEach((user) => {
        let newUser = userList.row.add([
            user.userid,
            user.username,
            (user.blacklist) ? '<span class="badge badge-danger">Personal Blacklist</span>' : '',
        ]);
        newUser.node().id = user.userid;
        newUser.node().addEventListener('click', function () {
            getUserDetails($(this))
        });
    });
    userList.draw(false);
}

function getUserDetails(userId) {
    // the first thing we do, so nothing is being executed on other users as well.
    $("div.widget-profile > div.panel-footer > div.btn-group > button").unbind().removeAttr('disabled');

    var skipParse = ["string", "number"];
    if (skipParse.indexOf(typeof userId) !== -1) {
        var user = PhotonDrop.UserManager.get(userId);
    } else {
        var user = PhotonDrop.UserManager.get($(userId).prop('id'));
    }
    //region fillUserInfo
    $("div.widget-profile > div.panel-heading > img.widget-profile-avatar").attr('src', `https://a.rsg.sc/n/${user.socialclub}/l`);
    $("div.widget-profile > div.panel-heading > h3.widget-profile-header > span.user-name").html(user.username);
    $("div.widget-profile > div.panel-heading > h3.widget-profile-header > span.user-id").html(user.userid);
    $("div.widget-profile > div.widget-profile-counters > a > span.attended-global").html(user.attended);
    $("div.widget-profile > div.widget-profile-counters > a > span.attended-mine").html(user.attended_internal.length);
    $("div.widget-profile > div.widget-profile-counters > a > span.user-warns").html(user.warns.length);
    $("div.widget-profile > div.list-group > span.list-group-item > span.user-socialclub").html(user.socialclub);
    $("div.widget-profile > div.list-group > span.list-group-item > span.user-blacklist").html((user.blacklist) ? '<span class="badge badge-danger">User is blacklisted</span>' : '<span class="badge badge-success">User is <b>not</b> blacklisted</span>');
    $("div.widget-profile > textarea.widget-profile-input").val(user.notes || "");
    $("div.widget-profile > div.panel-footer > div.btn-group > button").removeAttr('disabled');
    //endregion
    //region configure buttons
    $("div.widget-profile > div.panel-footer > div.btn-group > button.user-toggle-blacklist").on('click', () => {
        PhotonDrop.UserManager.blacklist(user.userid);
        populateUserList();
        getUserDetails(user.userid);
    });
    $("div.widget-profile > div.panel-footer > div.btn-group > button.user-delete").on('click', () => {
        PhotonDrop.Site.smalltalk.confirm('Confirmation required', `Do you really want to delete ${user.username}?`).then((result) => {
            PhotonDrop.UserManager.remove(user.userid);
            populateUserList();
            $("div.widget-profile").replaceWith(userInfo);
        }).catch(() => {});
    });

    $("div.widget-profile > div.panel-footer > div.btn-group > button.user-save").on('click', () => {
        PhotonDrop.Site.smalltalk.confirm('Confirmation required', `Do you really want to save changes to ${user.username}?`).then(() => {
            if (PhotonDrop.UserManager.edit(user.userid, {
                    username: $("div.widget-profile").find("span.user-name").text(),
                    attended: parseInt($("div.widget-profile").find("span.attended-global").text()),
                    socialclub: $("div.widget-profile").find("span.user-socialclub").text(),
                    notes: $("div.widget-profile > textarea.widget-profile-input").val()
                })) {
                PhotonDrop.Site.Alert.toast('success', 'Changes saved!', `Your changes to ${user.username} were successfully saved.`);
                populateUserList();
                getUserDetails(user.userid);
            } else {
                PhotonDrop.Site.Alert.toast('error', 'Uh oh!', `Your changes to ${user.username} could not be saved.`);
            }
        }).catch(() => {
            PhotonDrop.Site.smalltalk.alert('Aborted', `Changes to ${user.userName} reset.`);
        });
    });
    //endregion
    //region fillHistory
    var userHistory = $("div.user-history");
    userHistory.empty();
    let collectUserHistory = [];
    user.warns.forEach((warn) => {
        collectUserHistory.push({
            type: "warning",
            title: `${user.username} received a warning`,
            text: `<i>Reason: ${warn.reason}</i>`,
            time: warn.time,
            session: warn.sessionid
        });
    });
    user.attended_internal.forEach((drop) => {
        let sessionInfo = PhotonDrop.Storage.queryAll("session", {
            query: {
                ID: drop.sessionid
            },
            limit: 1
        })[0];

        collectUserHistory.push({
            type: "success",
            title: `${user.username} joined a session`,
            text: `${user.username} joined ${sessionInfo.type} lobby. Duration: ${sessionInfo.duration} minutes. Location: ${sessionInfo.location}`,
            time: sessionInfo.created_time,
            session: drop.sessionid
        });
    });

    collectUserHistory.sort(function (a, b) {
        if (a.time > b.time) {
            return 1;
        } else if (a.time < b.time) {
            return -1;
        }
        return 0;
    });

    collectUserHistory.forEach((hist) => {
        userHistory.prepend(`<div class="widget-activity-item">
        <div class="widget-activity-avatar">
            <img src="https://a.rsg.sc/n/${user.socialclub}/l" class="widget-profile-avatar">
            <div class="widget-activity-icon bg-${hist.type}"></div>
        </div>
        <div class="widget-activity-header">${hist.title}</div>
        <div class="widget-activity-text">
            ${hist.text}
        </div>
        <div class="widget-activity-footer">${moment(hist.time).fromNow()} in Session# ${hist.session}</div>
    </div>`);

    });

}