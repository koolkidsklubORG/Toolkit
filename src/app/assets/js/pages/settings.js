$(function () {
    $("input#threshold_money").val(PhotonDrop.Site.Settings().threshold.money);
    $("input#threshold_warns").val(PhotonDrop.Site.Settings().threshold.warns);
    $("select#announcements_with_mention").find(`option[value="${PhotonDrop.Site.Settings().announcements_with_mention}"]`).prop('selected', true);
    $("select#discord_preparing").find(`option[value="${PhotonDrop.Site.Settings().discord.preparing}"]`).prop('selected', true);
    //region populateFields
    if (localStorage.getItem('session_defaults')) {
        let sd = JSON.parse(localStorage.getItem('session_defaults'));
        $("div.session-defaults").find("select#session_type").find(`option[value="${sd.type}"]`).prop('selected', true);
        $("div.session-defaults").find("input#session_duration").val(sd.duration);
        $("div.session-defaults").find("input#session_location").val(sd.location);
    }
    populateTexts();
    populateThemes();
    populateScheme();
    getSCLoginState();
    //endregion

    //region textstuff
    $('#markdown').markdown({
        iconlibrary: 'fa',
        footer: '<div id="md-character-footer"></div><small id="md-character-counter" class="text-muted">350 character left</small>',
        hiddenButtons: ['cmdImage', 'cmdUrl', 'cmdList', 'cmdListO', 'cmdCode', 'cmdQuote'],
        savable: true,
        additionalButtons: [
            [{
                    name: "groupTime",
                    data: [{
                        name: "phStarted",
                        title: "Time since drop started",
                        icon: "fa fa-clock-o",
                        callback: function (e) {
                            var chunk, cursor,
                                selected = e.getSelection(),
                                content = e.getContent();
                            e.replaceSelection("%started%");
                        }
                    }]
                },
                {
                    name: "groupTime",
                    data: [{
                        name: "phRemaining",
                        title: "Remaining Time",
                        icon: "fa fa-clock-o",
                        callback: function (e) {
                            var chunk, cursor,
                                selected = e.getSelection(),
                                content = e.getContent();
                            e.replaceSelection("%remaining%");
                        }
                    }]
                },
                {
                    name: "groupTime",
                    data: [{
                        name: "phDuration",
                        title: "Duration",
                        icon: "fa fa-clock-o",
                        callback: function (e) {
                            var chunk, cursor,
                                selected = e.getSelection(),
                                content = e.getContent();
                            e.replaceSelection("%duration%");
                        }
                    }]
                },
                {
                    name: "groupLocation",
                    data: [{
                        name: "phLocation",
                        title: "Location",
                        icon: "fa fa-map-marker",
                        callback: function (e) {
                            var chunk, cursor,
                                selected = e.getSelection(),
                                content = e.getContent();
                            e.replaceSelection("%location%");
                        }
                    }]
                },
                {
                    name: "groupWarns",
                    data: [{
                        name: "phWarnsCur",
                        title: "User current warning count",
                        icon: "fa fa-exclamation",
                        callback: function (e) {
                            var chunk, cursor,
                                selected = e.getSelection(),
                                content = e.getContent();
                            e.replaceSelection("%warns_curr%");
                        }
                    }]
                },
                {
                    name: "groupWarns",
                    data: [{
                        name: "phWarnsMax",
                        title: "Maximum Warnings as defined in settings",
                        icon: "fa fa-exclamation-triangle",
                        callback: function (e) {
                            var chunk, cursor,
                                selected = e.getSelection(),
                                content = e.getContent();
                            e.replaceSelection("%warns_max%");
                        }
                    }]
                }
            ]
        ],
        onChange: function (e) {
            var contentLength = e.getContent().length;
            if (contentLength > 2000) {
                $('#md-character-counter')
                    .removeClass('text-muted')
                    .addClass('text-danger')
                    .html((contentLength - 2000) + ' character surplus.');
            } else {
                $('#md-character-counter')
                    .removeClass('text-danger')
                    .addClass('text-muted')
                    .html((2000 - contentLength) + ' character left.');
            }
        },

        onSave: saveText,
    });

    $('#markdown').trigger('change');
    //endregion
});
// region textFunctions
function populateTexts() {
    let texts = PhotonDrop.Texts.getTextsByScope("announcement");
    $("ul.texts-nav").empty();
    $("ul.texts-nav").append(`<li onClick="loadText(this);" data-id="new" class="list-group-item">Create new text</li>`);
    texts.forEach((text) => {
        $("ul.texts-nav").append(`<li onClick="loadText(this);" data-id="${text.ID}" class="list-group-item">${text.title} <span class="label label-info">${text.scope}</span></li>`);
    });
    texts = PhotonDrop.Texts.getTextsByScope("warning");
    texts.forEach((text) => {
        $("ul.texts-nav").append(`<li onClick="loadText(this);" data-id="${text.ID}" class="list-group-item">${text.title} <span class="label label-warning">${text.scope}</span></li>`);
    });
}

function loadText(e) {
    let textId = $(e).data('id');
    if (textId !== "new") {
        let textData = PhotonDrop.Texts.getTextById(textId);
        $("select#text-scope").find('option').each(() => $(this).removeProp('selected'));
        $("select#text-scope").find('option:contains("' + textData.scope + '")').prop('selected', true);
        $("input#text-title").val(textData.title);
        $("textarea#markdown").val(textData.content);
        $("textarea#markdown").data('id', textData.ID);
        $("textarea#markdown").trigger('change');
    } else {
        $("textarea#markdown").data('id', "new");
        $("input#text-title").val("New text");
        $("textarea#markdown").val("Use the editor buttons to quickly set placeholders.");
        $("textarea#markdown").trigger('change');
    }
}

function saveText(e) {
    let id = $(e.$element).data('id');
    let title = $("input#text-title").val();
    let text = e.getContent();
    let scope = $("select#text-scope").val();
    if (id === "new") {
        if (PhotonDrop.Texts.addText(title, text, scope)) {
            PhotonDrop.Site.Alert.toast("success", "Text saved", `Your new text ${title} was successfully saved.`);
        }
    } else {
        if (PhotonDrop.Texts.editTextById(id, title, text, scope)) {
            PhotonDrop.Site.Alert.toast("success", "Changes saved", `Your changes to text ${title} (ID: ${id}) were successfully saved.`);
        }
    }
    populateTexts();
}

function deleteText() {
    let text = {
        id: $("textarea#markdown").data('id'),
        title: $("input#text-title").val()
    };
    if (text.id !== "new") {
        PhotonDrop.Site.smalltalk.confirm('Are you sure?', `Are you sure you want to remove ${text.title} (ID: ${text.id}) from the database?`).then(() => {
            if (PhotonDrop.Texts.deleteTextById(text.id)) {
                PhotonDrop.Site.Alert.toast("success", "Text deleted", `The text ${text.title} (ID: ${text.id}) was removed from the database.`);
                $("textarea#markdown").data('id', "new");
                $("input#text-title").val("New text");
                $("textarea#markdown").val("Use the editor buttons to quickly set placeholders.");
                populateTexts();
            }
        });
    }
}
//endregion

//region generalSettings
function saveSettings() {
    let moneyThreshold = parseInt($("div.general-settings").find("input#threshold_money").val());
    let warnThreshold = parseInt($("div.general-settings").find("input#threshold_warns").val());
    let announcements_with_mention = $("div.general-settings").find("select#announcements_with_mention").val();
    localStorage.setItem("theme", $("select#app_theme").val());
    localStorage.setItem("soundscheme", $("select#app_soundscheme").val());
    localStorage.setItem("session_defaults", JSON.stringify({
        type: $("div.session-defaults").find("select#session_type").val(),
        duration: Number($("div.session-defaults").find("input#session_duration").val()),
        location: $("div.session-defaults").find("input#session_location").val()
    }));
    localStorage.setItem('settings', JSON.stringify({
        threshold: {
            warns: warnThreshold,
            money: moneyThreshold
        },
        discord: {
            preparing: $("div.discord-settings").find("select#discord_preparing").val()
        },
        announcements_with_mention
    }));
    PhotonDrop.Site.Alert.toast("success", "Settings saved", 'Your changes have been successfully saved. Theme changes require a full reload: <button onClick="window.location.reload();" class="btn btn-warning btn-xs">Reload now</button>');
}
//endregion

function populateThemes() {
    const themeDir = __dirname + '/assets/css/themes/';
    const fs = require('fs');
    const currentTheme = localStorage.getItem("theme");
    fs.readdirSync(themeDir).forEach((file) => {
        if (!file.includes(".rtl")) {
            let themeName = file.replace(".min.css", "");
            $("select#app_theme").append(`<option value="${themeName}" ${(themeName == currentTheme)?'selected':''}>${themeName}</option>`);
        }
    });
}

function populateScheme() {
    const schemeDir = __dirname + '/assets/sounds/';
    const fs = require('fs');
    const currentScheme = localStorage.getItem("soundscheme");
    fs.readdirSync(schemeDir).forEach((file) => {
        if (fs.lstatSync(schemeDir + file).isDirectory()) {
            $("select#app_soundscheme").append(`<option value="${file}" ${(file == currentScheme)?'selected':''}>${file}</option>`);
        }
    });
}

$("button#sclogin").on('click', (e) => {
    e.preventDefault();
    PhotonDrop.Site.smalltalk.confirm("Login to Social Club", "Please sign in to SocialClub and tick the 'Remember me' box to use SCLookup.").then(() => {
        window.open('https://socialclub.rockstargames.com');
    });
});

function resetApp() {
    PhotonDrop.Site.smalltalk.confirm("Reset Application", "This will completely reset the application. Including your saved texts, users and sessions. Do you really want to continue?").then(() => {
        localStorage.clear();
        window.location.reload();
    });
}

function backupData() {
    const {
        dialog
    } = require('electron').remote;
    const backupPath = dialog.showSaveDialog({
        title: "Save Database Backup",
        defaultPath: 'db_photondrop.json'
    });
    const fs = require('fs');
    if (backupPath) {
        fs.writeFile(backupPath, PhotonDrop.Storage.serialize(), 'utf8', function (err) {
            if (err) {
                PhotonDrop.Site.Alert.toast("error", "Backup could not be saved", err.message);
                return;
            }

            PhotonDrop.Site.Alert.toast("success", "Backup saved!", "The Backup was successfully saved.");
        });
    }
}

function restoreData() {
    const {
        dialog
    } = require('electron').remote;
    const backupPath = dialog.showOpenDialog({
        title: "Open Database Backup",
        defaultPath: 'db_photondrop.json',
        filters: [{
            name: 'JSON',
            extensions: ['json'],
            properties: ['openFile']
        }]
    });
    const fs = require('fs');
    const path = require('path').resolve(backupPath[0]);
    if (path) {
        fs.readFile(path, 'utf8', function (err, data) {
            if (err) {
                PhotonDrop.Site.Alert.toast("error", "Backup could not be restored", err.message);
                return;
            }
            localStorage.setItem("db_photondrop", data);
            PhotonDrop.Site.Alert.toast("success", "Backup restored!", "The Backup was successfully restored. Reloading app in 3 seconds...");
            setTimeout(() => {
                window.location.reload(true);
            }, 3000);
        });
    }
};

function getSCLoginState() {
    $.ajax({
        url: "https://socialclub.rockstargames.com/myprofile",
        method: 'GET',
        headers: {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.8,nl;q=0.6',
        }
    })
    .done(function (data) {
        $("div#scloginstate").html('You are already logged in!<br />User: <b>' + $(data).find("div#headerUsername > p").text() + '</b>');
        $("button#sclogin").html('Log Out / Switch User');
        $("button#sclogin").removeClass('btn-warning').addClass('btn-danger');
    })
    .fail(function (xhr) {
        $("div#scloginstate").html('You are <b>not</b> logged in. Or something went wrong.');
    });
}

function openFolder(f){
    const { shell, remote } = require('electron');
    const path = require('path');
    if(f==='themes') {
        shell.openItem(path.resolve(remote.app.getAppPath() + '\\src\\app\\assets\\css\\themes'));
    } else if(f === 'sounds') {
        shell.openItem(path.resolve(remote.app.getAppPath() + '\\src\\app\\assets\\sounds'));
    }
}